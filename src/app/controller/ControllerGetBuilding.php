<?php
/**
 * Created by PhpStorm.
 * User: bertr
 * Date: 13/03/2017
 * Time: 12:47
 */

namespace app\controller;


use app\model\Building;
use app\model\Comment;

class ControllerGetBuilding {

    private $params;

    public function __construct($p) {
        $this->params = $p;
    }

    public function getBuilding() {

        $building = Building::all()
            ->where('latitude', '=', $this->params['latitude'])
            ->where('longitude', '=', $this->params['longitude'])
            ->first()
            ->toArray();

        $comments = Comment::all()
            ->where('idBuilding', '=', $building["id"])
            ->toArray();

        header("Content-Type: application/json");
        echo json_encode(array_merge($building, $comments));

    }

}